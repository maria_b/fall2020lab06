package lab6;
//Maria Barba 1932657
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	
	private RpsGame theGame =new RpsGame();
	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		HBox buttons = new HBox();
		Button button1 = new Button("rock");
		Button button2 = new Button("scissors");
		Button button3 = new Button("paper");
		buttons.getChildren().addAll(button1,button2,button3);
		//Hbox for textfield
		HBox textField = new HBox();
		TextField welcome = new TextField("Welcome!");
		welcome.setPrefWidth(200);
		TextField wins = new TextField("wins:0");
		TextField losses = new TextField("losses:0");
		TextField ties = new TextField("ties:0");
		textField.getChildren().addAll(welcome,wins,losses,ties);
		
		//vbox that has everything

		VBox overall = new VBox();
		overall.getChildren().addAll(buttons,textField);
		
		root.getChildren().add(overall);
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 	
		stage.show(); 
		RpsChoice rock =new RpsChoice( welcome, wins, losses, ties, "Rock", theGame);
		button1.setOnAction(rock);
		RpsChoice scissors =new RpsChoice(welcome, wins, losses, ties, "Scissors", theGame);
		button2.setOnAction(scissors);
		RpsChoice paper =new RpsChoice(welcome, wins, losses, ties, "Paper", theGame);
		button3.setOnAction(paper);
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    



