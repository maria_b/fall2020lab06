package lab6;
//Maria Barba 1932657
import java.util.Random;
import java.util.Scanner;

public class RpsGame {
	private int numWins ;
	private int numTies ;
	private int losses ;
	private Random rand;
	
	public RpsGame() {
		this.numWins=0;
		this.numTies=0;
		this.losses=0;
		this.rand=new Random();;
	}

	public static void main(String[] args) {
		RpsGame game1=new RpsGame ();
		System.out.println("Start round one of rock paper scissors !");
		System.out.println("Player choose your symbol :");
		System.out.println("0-Rock , 1-Scissors, 2-Paper");
		Scanner myInput = new Scanner(System.in);
		int playercChoice = myInput.nextInt();
		String playerSymbol = null;
		switch (playercChoice) {
		case 0:
			playerSymbol = "Rock";
			break;
		case 1:
			playerSymbol = "Scissors";
			break;
		case 2:
			playerSymbol = "Paper";
			break;
		}
		
		System.out.println(game1.playRound(playerSymbol));
	}

	public int getNumWins() {
		return this.numWins;
	}

	public int getNumTies() {
		return this.numTies;
	}

	public int getLosses() {
		return this.losses;
	}

	public String playRound(String input) {
		int myRandom = rand.nextInt(3);
		String computerSymbol;
		if (myRandom == 0) {
			computerSymbol = "Rock";
		} else if (myRandom == 1) {
			computerSymbol = "Scissors";
		} else {
			computerSymbol = "Paper";
		}
//checks who won
		String gameStatement;
		if (input.equals("Rock") && computerSymbol.equals("Scissors")) {
			this.numWins++;
			gameStatement = "Computer plays " + computerSymbol + " and player won !";
		} else if (input.equals("Scissors") && computerSymbol.equals("Rock")) {
			this.losses++;
			gameStatement = "Computer plays " + computerSymbol + " and computer won !";
		} else if (input.equals("Scissors") && computerSymbol.equals("Paper")) {
			this.numWins++;
			gameStatement = "Computer plays " + computerSymbol + " and player won !";
		} else if (input.equals("Paper") && computerSymbol.equals("Scissors")) {
			this.losses++;
			gameStatement = "Computer plays " + computerSymbol + " and computer won !";
		} else if (input.equals("Paper") && computerSymbol.equals("Rock")) {
			this.numWins++;
			gameStatement = "Computer plays " + computerSymbol + " and player won !";
		} else if (input.equals("Rock") && computerSymbol.equals("Paper")) {
			this.losses++;
			gameStatement = "Computer plays " + computerSymbol + " and computer won !";
		} else {
			this.numTies++;
			// computer & player have the same symbol
			gameStatement = "Computer plays " + computerSymbol + " and no one won !";
		}
		return gameStatement;
	}
}
