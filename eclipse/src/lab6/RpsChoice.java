package lab6;
//Maria Barba 1932657

import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RpsChoice implements EventHandler<ActionEvent>  {
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String playerChoice;
	private RpsGame myGame;
	
	public RpsChoice(TextField message,TextField wins, TextField losses,TextField ties,String playerChoice,RpsGame myGame) {
		this.message=message;
		this.wins=wins;
		this.losses=losses;
		this.ties=ties;
		this.playerChoice=playerChoice;
		this.myGame=myGame;
	}
	@Override
	public void handle(ActionEvent e) {
		String gameResult=myGame.playRound(playerChoice);
		message.setText(gameResult);
		int numberOfWins=myGame.getNumWins();
		int numberOfTies=myGame.getNumTies();
		int numberOfLosses=myGame.getLosses();
		wins.setText("wins : "+numberOfWins);
		ties.setText("ties : "+numberOfTies);
		losses.setText("losses : "+numberOfLosses);
	}

}
